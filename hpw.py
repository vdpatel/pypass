import sys
import hashlib

arguments = sys.argv
#print(arguments)
mp = ""
sites = ""
hasSpec = True
argLen = len(arguments)

if argLen > 3:
    mp = arguments[1]
    if arguments[2] != 'True':
        hasSpec = False
    sites = arguments[3]
elif argLen == 2:
    mp = arguments[1]
    sites = arguments[2]
else:
    mp = "salt"
    sites = "go"

#for i in hashlib.algorithms_available:
#    print(i)

cha = []

if hasSpec == True:
    cha = ['!', '@', '#', '$', '%']


for i in range(48,58):
    cha.append(chr(i))

if hasSpec ==True:
    cha2 = ['!', '@', '#', '$', '%']
    for i in cha2:
        cha.append(i)

for i in range(65,91):
    cha.append(chr(i))
    #32 is the base10 value difference from lower to upper casw
    cha.append(chr(i+32))

toHash = (sites + mp).encode('utf-8')
strArray = list(hashlib.md5(toHash).hexdigest())

arrLen = len(strArray)

password = ''
for i in range(0, arrLen, 2):
    x = int(strArray[i+1], 16) + int(strArray[i], 16)
    #print(x)
    if x > len(cha):
        #print("---" + str(x % len(cha)))
        password = password + cha[x % len(cha)]
    if x <= len(cha):
        #print("---" + str(x) + '---' + str(cha[x]))
        password = password + cha[x]

    #print(i)
print(sites + ' ' + password)

