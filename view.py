import sys
import os

def scriptCall(x):
    os.system(x)

arguments = sys.argv
argLen = len(arguments)
script = "hpw.py"

#print(str(arguments) + ' ' + str(argLen))

mp = ""
hasSpec = True
site = ""

mp = arguments[1]

if argLen > 3 and (arguments[2] == 'True' or arguments[2] == 'False'):
    if arguments[2] != 'True':
        hasSpec = False
    for i in range(3, argLen):
        site = arguments[i]
        runScript = script + ' ' + mp + ' ' + str(hasSpec) + ' ' + site
        #print(hasSpec)
        scriptCall(runScript)
elif argLen > 3 and (arguments[2] != 'True' or arguments[2] != 'False'):
    for i in range(2, argLen):
        site = arguments[i]
        runScript = script + ' ' + mp + ' ' + str(hasSpec) + ' ' + site
        scriptCall(runScript)
elif argLen == 3:
    site = arguments[2]
    runScript = script + ' ' + mp + ' ' + str(hasSpec) + ' ' + site
    scriptCall(runScript)
else:
    mp = 'Hello'
    site = 'sijdd'
    runScript = script + ' ' + mp + ' ' + str(hasSpec) + ' ' + site
    #print(hasSpec)
    scriptCall(runScript)
